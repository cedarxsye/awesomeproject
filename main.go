package main

// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.

import (
	"fmt"
	"sort"
)

type RangeList struct {
	list []int
}

func (rangeList *RangeList) getBound(left, right int) (int, int) {
	// binary search to find insertion index to maintain sort order
	i := sort.Search(len(rangeList.list), func(i int) bool { return rangeList.list[i] >= left })
	j := sort.Search(len(rangeList.list), func(j int) bool { return rangeList.list[j] > right })
	return i, j
}

func (rangeList *RangeList) modifyRange(leftInsertIndex int, rightInsertIndex int, indexList []int) {
	rangeList.list = rangeList.list[:leftInsertIndex+
		copy(rangeList.list[leftInsertIndex:], rangeList.list[rightInsertIndex:])]
	rangeList.list = append(rangeList.list[:leftInsertIndex], append(indexList, rangeList.list[leftInsertIndex:]...)...)
}

func (rangeList *RangeList) Add(rangeElement [2]int) {
	left := rangeElement[0]
	right := rangeElement[1]
	leftInsertIndex, rightInsertIndex := rangeList.getBound(left, right)
	var indexList []int
	// Since the subrange is made up of 2 numbers, an even index means not overlapping with an existing range
	if leftInsertIndex%2 == 0 {
		indexList = append(indexList, left)
	}
	if rightInsertIndex%2 == 0 {
		indexList = append(indexList, right)
	}
	rangeList.modifyRange(leftInsertIndex, rightInsertIndex, indexList)
}

func (rangeList *RangeList) Remove(rangeElement [2]int) {
	left := rangeElement[0]
	right := rangeElement[1]
	leftInsertIndex, rightInsertIndex := rangeList.getBound(left, right)
	var indexList []int
	if leftInsertIndex%2 == 1 {
		indexList = append(indexList, left)
	}
	if rightInsertIndex%2 == 1 {
		indexList = append(indexList, right)
	}
	rangeList.modifyRange(leftInsertIndex, rightInsertIndex, indexList)
}
func (rangeList *RangeList) Print() {
	for i := 0; i < len(rangeList.list); i += 2 {
		fmt.Printf("[%d, %d) ", rangeList.list[i], rangeList.list[i+1])
	}
	fmt.Println()
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
